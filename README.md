# Hits Algorithm

Algorithm diagram:

    1. Setting intermediary ratings and authority ratings of all vertices equal to one.
    
    2. Updating the credibility assessment.

    3. Updating the intermediary assessment.
    
    4. Normalization of values by dividing each intermediary score by the square root of the sum of the squares of all intermediary scores, and dividing each authority score by the square root of the sum of the squares of all authority scores.
    
    5. Repeat from the second step until the completion condition is met.
    
Input data format: a list of adjacency and the values of intermediary evaluation and authority evaluation for each vertex of the graph.
