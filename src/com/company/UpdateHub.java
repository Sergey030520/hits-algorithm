package com.company;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class UpdateHub {
    private final Configuration configuration;
    private final Path path_header;

    public boolean run(String input, String output) throws IOException, InterruptedException, ClassNotFoundException {
        Job job_upd_hub = Job.getInstance(configuration, "updating_hub");
        job_upd_hub.setJarByClass(Main.class);

        job_upd_hub.setMapperClass(MapperHubRating.class);
        job_upd_hub.setReducerClass(ReducerHubRating.class);

        job_upd_hub.setOutputKeyClass(Text.class);
        job_upd_hub.setOutputValueClass(Graph.class);

        FileInputFormat.addInputPath(job_upd_hub, new Path(path_header + input));
        FileOutputFormat.setOutputPath(job_upd_hub, new Path(path_header + output));
        return !job_upd_hub.waitForCompletion(true);
    }
    public UpdateHub(Configuration inConf, Path inPath){
        configuration = inConf;
        path_header = inPath;
    }
}
