package com.company;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class UpdateAuth {
    private final Configuration configuration;
    private final Path path_header;

    public boolean run(String input, String output) throws IOException, InterruptedException, ClassNotFoundException {
        Job job_upd_ath = Job.getInstance(configuration, "updating_auth");
        job_upd_ath.setJarByClass(Main.class);

        job_upd_ath.setMapperClass(MapperAuthRating.class);
        job_upd_ath.setReducerClass(ReducerAuthRating.class);

        job_upd_ath.setOutputKeyClass(Text.class);
        job_upd_ath.setOutputValueClass(Graph.class);

        FileInputFormat.addInputPath(job_upd_ath, new Path(path_header + input));
        FileOutputFormat.setOutputPath(job_upd_ath, new Path(path_header + output));
        return !job_upd_ath.waitForCompletion(true);
    }
    public UpdateAuth(Configuration inConf, Path inPath){
        configuration = inConf;
        path_header = inPath;
    }
}
