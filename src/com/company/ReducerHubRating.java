package com.company;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Objects;

public class ReducerHubRating extends Reducer<Text, Graph, Text, Text> {
    public void reduce(Text key, Iterable<Graph> values, Context context)
            throws IOException, InterruptedException {
        Graph header = new Graph();
        float hub = 0;
        StringBuilder link = new StringBuilder();
        for (Graph graph : values) {
            if(Objects.equals(graph.getHeader(), key.toString())){
                header = new Graph(graph.getHeader(), graph.getAuth(), 0, graph.getRef());
            }else{
                hub += graph.getAuth();
                link.append(graph.getHeader()).append(" ");
            }
        }
        header.setHub(hub);
        context.write(key, new Text(header.getAuth() + " " + header.getHub() + ";" + link));
    }
}