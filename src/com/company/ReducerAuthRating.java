package com.company;


import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Objects;

public class ReducerAuthRating extends Reducer<Text, Graph, Text, Text> {
    public void reduce(Text key, Iterable<Graph> values, Context context)
            throws IOException, InterruptedException {
        Graph header = new Graph(key.toString());
        StringBuilder stringBuilder = new StringBuilder();
        for (Graph link : values) {
            if(Objects.equals(link.getHeader(), key.toString()))
            {
                header.setHub(link.getHub());
                header.setRef(link.getRef());
            }else{
                stringBuilder.append(link.getHeader()).append(" ");
                header.setAuth(link.getHub()  + header.getAuth());
            }
        }
        context.write(key, new Text(header.getAuth() + " " + header.getHub() + ";" + stringBuilder));
    }
}