package com.company;


import com.company.Normalization.Normalization;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class Main {
    public static void loadData(Configuration configuration, Path path) throws IOException {
        FileSystem fileSystem = FileSystem.get(configuration);
        fileSystem.mkdirs(path);
        fileSystem.mkdirs(new Path(path.toString() + "/input/"));
        fileSystem.delete(new Path(path + "/result/"), true);
        fileSystem.copyFromLocalFile( new Path("src/com/company/dataset"), new Path(path + "/input/"));
        fileSystem.mkdirs(new Path(path + "/result/"));
    }
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Path path_header = new Path("/user/sergeimakarov/HitsAlgorithm/");

        org.apache.hadoop.conf.Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://0.0.0.0:9000/");

        loadData(configuration, path_header);

        UpdateAuth updateAuth = new UpdateAuth(configuration, path_header);
        UpdateHub updateHub = new UpdateHub(configuration, path_header);
        Normalization normalization = new Normalization(configuration, path_header);
        for (int i = 0; i < 5; ++i) {
            if (updateAuth.run((i == 0 ? "/input/dataset" : "/result/iteration_normalization_hub" + (i-1)),
                    "/result/iteration_upd_ath"+ i))
                System.exit(1);
            if(normalization.run("/result/iteration_upd_ath" + i, "/result/iteration_normalization_ath" + i,
                    true))
                System.exit(1);
            if (updateHub.run("/result/iteration_normalization_ath" + i, "/result/iteration_upd_hub" + i))
                System.exit(1);
            if(normalization.run("/result/iteration_upd_hub" + i, "/result/iteration_normalization_hub" + i,
                    false))
                System.exit(1);
        }
    }
}
