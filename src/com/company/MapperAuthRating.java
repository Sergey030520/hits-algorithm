package com.company;

import com.company.Graph;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class MapperAuthRating extends Mapper<Object, Text, Text, IntWritable> {
    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] parameters = value.toString().split(";");
        String header = parameters[0].split("\t")[0];
        String[] parameters_header = parameters[0].split("\t")[1].split(" ");
        float auth = Float.parseFloat(parameters_header[0]);
        float hub = Float.parseFloat(parameters_header[1]);
        for (String ref : parameters[1].split(" ")) {
            context.write(new Text(ref), new Graph(header, auth, hub));
        }
        Graph headerGraph = new Graph(header, auth, hub);
        headerGraph.setRef(new ArrayList<>(Arrays.asList(parameters[1].split(" "))));
        context.write(new Text(header), headerGraph);
    }
}