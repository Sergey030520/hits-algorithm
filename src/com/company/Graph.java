package com.company;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;


public class Graph extends IntWritable implements Writable{
    private String header;
    private float auth;
    private float hub;
    private ArrayList<String> ref;

    public Graph(){
        auth = 0;
        hub = 0;
        header = "";
        ref = new ArrayList<>(1);
    }
    public Graph(String inHeader, float inAuth, float inHub){
        header = inHeader;
        auth = inAuth;
        hub = inHub;
        ref = new ArrayList<>(1);
    }
    public Graph(String inHeader, float inAuth, float inHub, ArrayList<String> inRef){
        header = inHeader;
        auth = inAuth;
        hub = inHub;
        ref = inRef;
    }
    public Graph(String inHeader){
        header = inHeader;
        auth = 0;
        hub = 0;
        ref = new ArrayList<>(1);
    }

    public void addRef(String ref){
        this.ref.add(ref);
    }

    public void setRef(ArrayList<String> ref) {
        this.ref = ref;
    }

    public void setAuth(float auth) {
        this.auth = auth;
    }
    public void setHub(float hub) {
        this.hub = hub;
    }

    public float getAuth() {
        return auth;
    }

    public float getHub() {
        return hub;
    }

    public String getHeader() {
        return header;
    }

    public ArrayList<String> getRef() {
        return ref;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(header);
        dataOutput.writeFloat(auth);
        dataOutput.writeFloat(hub);
        dataOutput.writeInt(ref.size());
        if(ref.size() > 0) {
            for (String name : ref) {
                dataOutput.writeUTF(name);
            }
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        header = dataInput.readUTF();
        auth = dataInput.readFloat();
        hub = dataInput.readFloat();
        ref = new ArrayList<>();
        int countRef = dataInput.readInt();
        if(countRef > 0) {
            for (int numbRef = 0; numbRef < countRef; ++numbRef) {
                ref.add(dataInput.readUTF());
            }
        }
    }
}
