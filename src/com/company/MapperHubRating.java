package com.company;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


public class MapperHubRating extends Mapper<Object, Text, Text, IntWritable> {
    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] split_text = value.toString().split(";");
        String header_name = split_text[0].split("\t")[0];
        String[] parameters_header = split_text[0].split("\t")[1].split(" ");
        float auth = Float.parseFloat(parameters_header[0]);
        float hub = Float.parseFloat(parameters_header[1]);
        Graph graph_header = new Graph(header_name, auth, hub);
        for (String ref : split_text[1].split(" ")) {
            context.write(new Text(ref), graph_header);
        }
        context.write(new Text(header_name), graph_header);
    }
}