package com.company.Normalization;

import com.company.Graph;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class Normalization {
    private final Configuration configuration;
    private final Path path_header;

    private void creteOutputDir() throws IOException {
        FileSystem fileSystem = FileSystem.get(configuration);
        fileSystem.delete(new Path(path_header + "/result/normalization/"), true);
        fileSystem.mkdirs(new Path(path_header + "/result/normalization/"));
        fileSystem.close();
    }
    public boolean run(String input, String output,  Boolean normalizeAuth)
                        throws IOException, InterruptedException, ClassNotFoundException {
        creteOutputDir();
        configuration.set("isNormalizeAuth", Boolean.toString(normalizeAuth));

        Job normalizationSum = Job.getInstance(configuration, "normalization_sum");
        normalizationSum.setJarByClass(Normalization.class);

        normalizationSum.setMapperClass(MapperNormalizationSum.class);
        normalizationSum.setReducerClass(ReducerNormalizationSum.class);

        normalizationSum.setOutputKeyClass(Text.class);
        normalizationSum.setOutputValueClass(FloatWritable.class);

        FileInputFormat.addInputPath(normalizationSum, new Path(path_header + input));
        FileOutputFormat.setOutputPath(normalizationSum, new Path(path_header + "/result/normalization/generate_sum"));

        if(!normalizationSum.waitForCompletion(true)){
            return true;
        }
        Job normalizationDiv = Job.getInstance(configuration, "normalization_div");
        normalizationDiv.setJarByClass(Normalization.class);

        normalizationDiv.setMapperClass(MapperNormalizationDiv.class);
        normalizationDiv.setReducerClass(ReducerNormalizationDiv.class);

        normalizationDiv.setOutputKeyClass(Text.class);
        normalizationDiv.setOutputValueClass(Graph.class);

        FileInputFormat.addInputPath(normalizationDiv, new Path(path_header + input));
        FileOutputFormat.setOutputPath(normalizationDiv, new Path(path_header + output));
        return !normalizationDiv.waitForCompletion(true);
    }
    public Normalization(Configuration inConf, Path inPath) {
        configuration = inConf;
        path_header = inPath;
    }
}
