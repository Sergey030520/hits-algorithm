package com.company.Normalization;

import com.company.Graph;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ReducerNormalizationDiv extends Reducer<Text, Graph, Text, Text> {
    public void reduce(Text key, Iterable<Graph> values, Context context)
            throws IOException, InterruptedException {
        StringBuilder ref = new StringBuilder();
        Graph header = new Graph();
        for (Graph copyGraph: values){
            header = new Graph(copyGraph.getHeader(), copyGraph.getAuth(),
                    copyGraph.getHub(), copyGraph.getRef());
        }
        for (String link : header.getRef()){
            ref.append(link).append(" ");
        }
        context.write(key, new Text(header.getAuth() + " " + header.getHub() + ";" + ref));
    }
}
