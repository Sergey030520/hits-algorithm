package com.company.Normalization;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;

public class ReducerNormalizationSum extends Reducer<Text, FloatWritable, Text, Text> {
        public void reduce(Text key, Iterable<FloatWritable> values, Context context)
                throws IOException, InterruptedException {
                float sum = 0;
                for (FloatWritable value : values){
                        sum += Math.pow(value.get(), 2);
                }
                context.write(key, new Text(Float.toString(sum)));
        }
}

/*
public ArrayList<Graph> normalization(ArrayList<Graph> headers){
                float sumHub = 0, sumAth = 0;
                for (Graph header : headers){
                        sumHub += Math.pow(header.getHub(), 2);
                        sumAth += Math.pow(header.getAuth(), 2);
                }
                for (Graph graph : headers){
                        graph.setHub(graph.getAuth()/((float)Math.sqrt(sumAth)));
                        graph.setAuth(graph.getHub()/((float)Math.sqrt(sumHub)));
                }
                return headers;
        }
 */