package com.company.Normalization;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;


public class MapperNormalizationSum extends Mapper<Object, Text, Text, FloatWritable> {
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        Configuration configuration = context.getConfiguration();
        boolean isNormalizeAuth = Boolean.parseBoolean(configuration.get("isNormalizeAuth"));
        String[] split_text = value.toString().split(";");
        String[] parameters_header = split_text[0].split("\t")[1].split(" ");
        if(isNormalizeAuth) {
            context.write(new Text("auth"),
                    new FloatWritable((float) Math.pow(Float.parseFloat(parameters_header[0]), 2)));
        }else{
            context.write(new Text("hub"),
                    new FloatWritable((float) Math.pow(Float.parseFloat(parameters_header[1]), 2)));
        }
    }
}