package com.company.Normalization;

import com.company.Graph;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class MapperNormalizationDiv extends Mapper<Object, Text, Text, IntWritable> {
    private Map.Entry<String, Float> divider;
    @Override
    protected void setup(Mapper.Context context) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://0.0.0.0:9000/");
        FileSystem fileSystem = FileSystem.get(configuration);
        FSDataInputStream open = fileSystem.open(
                new Path("/user/sergeimakarov/HitsAlgorithm/result/normalization/generate_sum/part-r-00000"));
        BufferedReader bf = new BufferedReader(new InputStreamReader(open));
        for (String line; (line = bf.readLine()) != null; ) {
            if (!line.equals("")) {
                String[] parseStr = line.split(String.valueOf((char)9));
                divider = new AbstractMap.SimpleEntry<>(parseStr[0], Float.parseFloat(parseStr[1]));
            }
        }
        bf.close();
        open.close();
    }

    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] parameters = value.toString().split(";");
        String header = parameters[0].split("\t")[0];
        String[] parameters_header = parameters[0].split("\t")[1].split(" ");
        float auth = Float.parseFloat(parameters_header[0]), hub = Float.parseFloat(parameters_header[1]);
        if(Objects.equals(divider.getKey(), "auth")){
            auth /= Math.pow(divider.getValue(), 2);
        }else{
            hub /= Math.pow(divider.getValue(), 2);
        }
        Graph headerGraph = new Graph(header, auth, hub);
        headerGraph.setRef(new ArrayList<>(Arrays.asList(parameters[1].split(" "))));
        context.write(new Text(header), headerGraph);
    }
}
